const MicToSpeech = require('mic-to-speech');
const fs = require('fs');
 
let micToSpeech = new MicToSpeech();
 
micToSpeech.on('speech', function(rawAudio) {
  // create filename
  let now = new Date();
  let filename = (now.getMonth() + 1) + "-" + now.getDate()
    + "-" + now.getFullYear() + ' ' + now.getHours()
    + ':' + now.getMinutes() + ':' + now.getSeconds() + '.raw';
 
    // write to a file and restart speech detection
    fs.writeFile(filename, rawAudio, function() {
        console.log('saved: ' + filename);
        micToSpeech.resume();
    });
});
 
micToSpeech.start();
console.log('Listening for speech');